const User = require('../models/user.model');
const jwt = require('jsonwebtoken');
const redis_client = require('../helpers/redis_connect');
const bcrypt = require("bcryptjs");
const user_helper = require('../helpers/user_helper');

async function Register(req, res) {
    const { name, password, contact, address, gender, country } = req.body;
    if (!(name && password && contact && address && gender && country)) {
        res.status(400).send("Required inputs are got missing");
    }
    const existingUser = await User.findOne({ name: name });
    if (existingUser) {
        return res.status(409).send("User Already Exists in system. Please Login");
    }
    salt = await bcrypt.hash(password, 10);
    try {
        const userObj = {
            name: name,
            password: salt,
            contact: contact,
            address: address,
            gender: gender,
            country: country,
        };
        const user = await User.create(userObj);
        console.log('user', user);
        const access_token = jwt.sign({ sub: user._id }, process.env.JWT_ACCESS_SECRET, { expiresIn: process.env.JWT_ACCESS_TIME });
        console.log('access_token', access_token);
        const refresh_token = GenerateRefreshToken(user._id);
        user_helper.addUserData(userObj);
        return res.json({ status: true, message: "Registered successfully.", data: { access_token, refresh_token } });
    } catch (error) {
        return res.status(400).json({ status: false, message: "Something went wrong, check with your adminstartion", data: error });
    }
}

async function Login(req, res) {
    const { name, password } = req.body;
    if (!(name && password)) {
        return res.status(400).send("Required fields are missed");
    }
    try {
        let pswdMatch = false;
        const user = await User.findOne({ name: name }).exec();
        if (user)
            pswdMatch = await bcrypt.compare(password, user.password);
        if (user === null || (user && !pswdMatch)) res.status(401).json({ status: false, message: "username or password is not valid." });
        console.log('user', user);
        const access_token = jwt.sign({ sub: user._id }, process.env.JWT_ACCESS_SECRET, { expiresIn: process.env.JWT_ACCESS_TIME });
        console.log('access_token', access_token);
        const refresh_token = GenerateRefreshToken(user._id);
        return res.json({ status: true, message: "login success", data: { access_token, refresh_token } });
    } catch (error) {
        return res.status(401).json({ status: true, message: "login fail", data: error });
    }
}

function Logout(req, res) {
    const user_id = req.userData.sub;
    const token = req.token;
    redis_client.del(user_id.toString());
    redis_client.set('BLACKLIST_' + user_id.toString(), token);
    return res.json({ status: true, message: "success." });
}

function GetAccessToken(req, res) {
    const user_id = req.userData.sub;
    const access_token = jwt.sign({ sub: user_id }, process.env.JWT_ACCESS_SECRET, { expiresIn: process.env.JWT_ACCESS_TIME });
    const refresh_token = GenerateRefreshToken(user_id);
    return res.json({ status: true, message: "success", data: { access_token, refresh_token } });
}

function GenerateRefreshToken(user_id) {
    const refresh_token = jwt.sign({ sub: user_id }, process.env.JWT_REFRESH_SECRET, { expiresIn: process.env.JWT_REFRESH_TIME });
    redis_client.get(user_id.toString(), (err, data) => {
        if (err) throw err;
        redis_client.set(user_id.toString(), JSON.stringify({ token: refresh_token }));
    })
    return refresh_token;
}

module.exports = {
    Register,
    Login,
    Logout,
    GetAccessToken
}