const elastic_helper = require('../helpers/elastic_helper');

async function addUserData(user) {
    try {
        const userObject = {
            name: user['name'],
            contact: user['contact'],
            address: user['address']
        };
        const res = await elastic_helper.checkUsersIndices();
        if (res['status']) {
            await elastic_helper.addUserDataToES(userObject);
        }
        else {
            console.error(`Error Ocurred while checking the user index`, JSON.stringify(res['error']));
        }
    } catch (e) {
        console.log(`Error Ocurred while adding the users data to elastic search`, JSON.stringify(e));
    }
}

module.exports = {
    addUserData
}
