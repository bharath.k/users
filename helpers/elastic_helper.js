const common_helper = require('../helpers/common_helper');

async function checkUsersIndices() {
    const ret = {};
    const baseurl = process.env.ELASTIC_SEARCH + '/users';
    const res = await common_helper.restCall(baseurl, {});
    if (res['data']['error'] && res['data']['error']['error']['type'] == 'index_not_found_exception') {
        {
            console.log('index not found creating..!');
            const property = {
                mappings: {
                    doc: {
                        properties: {
                            name: { type: 'string' },
                            contact: { type: "string" },
                            address: { type: "string" }
                        }
                    }
                }
            };
            const curl_respone = await common_helper.restCall(baseurl, property, 'POST');
            const response = curl_respone['data'];
            if (response['acknowledged']) {
               ret['status'] = true;
            }
            else {
                ret['status'] = false;
                ret['error'] = res['data']['error'] && res['data']['error']['error']['type'] ? res['data']['error']['error']['type'] : undefined;
            }
        }

    } else {
        console.log('user index already exists');
        ret['status'] = true;
    }
    return ret;
}

async function addUserDataToES(data) {
    try {
        const baseurl = process.env.ELASTIC_SEARCH + `/users/doc/${data['name']}`
        return await common_helper.restCall(baseurl, data, 'POST');
    } catch (e) {
        console.error("Error Ocurred while adding the users data to elastic search", JSON.stringify(e));
    }
}

async function usersSuggestions(req_term) {
    const search_data = {};
    let i = 0;
    if (!req_term) {
        return search_data;
    }
    let term = req_term.trim();
    term = common_helper.prepareElasticSearchTerm(term);
    const data = {
        "fields": ["name", "contact", "address"],
        "query": { "query_string": { "query": term, "fields": ["name^1", "contact^1"] } },
        "sort": { "name": { "order": "desc" } },
        "size": 10
    };
    const baseurl = process.env.ELASTIC_SEARCH + '/users/doc/_search';
    const curl_respone = await common_helper.restCall(baseurl, data, 'POST');
    const response = curl_respone['data'];
    if (response['hits'] && response['hits']['total']) {
        response['hits']['hits'].forEach(tag => {
            search_data[i] = {};
            search_data[i]['name'] = tag['fields']['name'] && tag['fields']['name'][0] ? tag['fields']['name'][0] : "";
            search_data[i]['contact'] = tag['fields']['contact'] && tag['fields']['contact'][0] ? tag['fields']['contact'][0] : "";
            search_data[i]['address'] = tag['fields']['address'] && tag['fields']['address'][0] ? tag['fields']['address'][0] : "";
            i = i + 1;
        });
    }
    return search_data;
}

module.exports = {
    checkUsersIndices,
    addUserDataToES,
    usersSuggestions
}

