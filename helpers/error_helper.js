function getErrorData(err) {
    let errorData = (err['error'] && err['error']['message']) ? err['error']['message'] : undefined;
    if (errorData) {
        errorData = (err['message'] && err['message']['errors']) ? err['message']['errors'] : undefined;
    }
    if (!errorData) {
        errorData = err;
    }
    return errorData;
}

module.exports = {
    getErrorData
}