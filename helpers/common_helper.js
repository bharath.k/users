const error_helper = require('../helpers/error_helper');
const request = require('request-promise');

function restCall(apiUrl, payload = {}, method = 'GET', type = 'body', returnOutput = false) {
    console.log(`_restCall ${apiUrl} ${method}`);
    return new Promise(resolve => {
        try {
            const options = {
                method: method,
                uri: apiUrl,
                json: true
            };
            options[type] = payload;
            request(options).then(output => {
                console.log('Success response from api');
                if (returnOutput) {
                    resolve({ code: 200, data: output });
                } else {
                    if (!output || output['error'] || (output['message'] && output['message']['errors'])) {
                        resolve({
                            code: 400,
                            data: error_helper.getErrorData(output)
                        });
                    } else {
                        resolve({ code: 200, data: output });
                    }
                }
            }).catch((err) => {
                console.error('Error response from api', JSON.stringify(err));

                resolve({
                    code: 400,
                    data: error_helper.getErrorData(err)
                });
            });
        } catch (error) {
            console.error('Failed to invoke rest api', JSON.stringify(error));
            resolve({ code: 500, data: error.toString() });
        }
    });
}

function prepareElasticSearchTerm(term) {
    term = term.replace(" ", "* AND ");
    term = "*" + term.replace("-", "* AND ") + "*";
    return term;
}

module.exports = {
    restCall,
    prepareElasticSearchTerm
}