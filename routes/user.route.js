const route = require('express').Router();
const auth_middleware = require('../helpers/auth.middleware');
const elastic_helper = require('../helpers/elastic_helper');

route.post('/search', auth_middleware.verifyToken, async (req, res) => {
    const term = req.body.term;
    const users = await elastic_helper.usersSuggestions(term);
    return res.json({status: true, message: users});
});

module.exports = route;