require('dotenv').config();
const express = require('express');
const app = express();

const mongoose = require('mongoose');

mongoose.connect(
    process.env.DB_CONN_STRING,
    { useUnifiedTopology: true, useNewUrlParser: true},
    () => console.log('connected to mongodb.')
);

app.use(express.json());

const auth_routes = require('./routes/auth.route');
const user_routes = require('./routes/user.route');

app.use('/auth', auth_routes);
app.use('/user', user_routes);

const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT || 8084;

app.listen(port, () => console.log('server is running..'));